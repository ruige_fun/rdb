package rdb

import (
	"context"
	"fmt"
	"github.com/redis/go-redis/v9"
	"sync"
)

type ZSet interface {
	TopicKey() string
	RedisClient() *redis.Client
	Add(key string, score float64) error
	Adds(keys map[string]float64) error
	Del(key string) error
	Dels(keys []string) error
	ZpopMin() (ZsetItem, error)
	ZpopMax() (ZsetItem, error)
	GetByIndex(index int64) (ZsetItem, error)
	GetByIndexRange(start int64, end int64) ([]ZsetItem, error)
	GetByIndexDESC(index int64) (ZsetItem, error)
	GetByIndexRangeDESC(start int64, end int64) ([]ZsetItem, error)
	GetByKey(key string) (ZsetItem, error)
	Count() (int64, error)
	GetAllKey() ([]ZsetItem, error)
	ClearAll() error
}

type ZsetItem struct {
	Key   string
	Score float64
}

type _zset struct {
	Topic string        //主题
	Redis *redis.Client //Redis客户端
	RWL   sync.RWMutex  //读写锁
}

func NewZSet(client *redis.Client, topic string) ZSet {
	return &_zset{
		Topic: topic,
		Redis: client,
		RWL:   sync.RWMutex{},
	}
}

// TopicKey 返回默认生成的主题key
func (s *_zset) TopicKey() string {
	return fmt.Sprint("ZSet_", s.Topic)
}

// RedisClient 返回绑定的Redis客户端
func (s *_zset) RedisClient() *redis.Client {
	return s.Redis
}

// Add 添加一个key
func (s *_zset) Add(key string, score float64) error {
	s.RWL.Lock()
	err := s.Redis.ZAdd(context.Background(), s.TopicKey(), redis.Z{
		Score:  score,
		Member: key,
	}).Err()
	s.RWL.Unlock()
	return err
}

// Adds 添加多个key
func (s *_zset) Adds(keys map[string]float64) error {
	s.RWL.Lock()
	var temp []redis.Z
	for k, v := range keys {
		temp = append(temp, redis.Z{
			Score:  v,
			Member: k,
		})
	}
	err := s.Redis.ZAdd(context.Background(), s.TopicKey(), temp...).Err()
	s.RWL.Unlock()
	return err
}

// Del 删除指定的key
func (s *_zset) Del(key string) error {
	s.RWL.Lock()
	err := s.Redis.ZRem(context.Background(), s.TopicKey(), key).Err()
	s.RWL.Unlock()
	return err
}

// Dels 删除指定的keys
func (s *_zset) Dels(keys []string) error {
	s.RWL.Lock()
	var temp []any
	for _, d := range keys {
		temp = append(temp, d)
	}
	err := s.Redis.ZRem(context.Background(), s.TopicKey(), temp...).Err()
	s.RWL.Unlock()
	return err
}

// ZpopMin 获取并且删除最小分数的
func (s *_zset) ZpopMin() (ZsetItem, error) {
	s.RWL.Lock()
	result, err := s.Redis.ZPopMin(context.Background(), s.TopicKey()).Result()
	s.RWL.Unlock()
	if len(result) <= 0 {
		return ZsetItem{}, fmt.Errorf(fmt.Sprint(err))
	}
	return ZsetItem{
		Key:   fmt.Sprint(result[0].Member),
		Score: result[0].Score,
	}, err
}

// ZpopMax 获取并且删除最大分数的
func (s *_zset) ZpopMax() (ZsetItem, error) {
	s.RWL.Lock()
	result, err := s.Redis.ZPopMax(context.Background(), s.TopicKey()).Result()
	s.RWL.Unlock()
	if len(result) <= 0 {
		return ZsetItem{}, fmt.Errorf(fmt.Sprint(err))
	}
	return ZsetItem{
		Key:   fmt.Sprint(result[0].Member),
		Score: result[0].Score,
	}, err
}

// GetByIndex 获取指定分数排名的成员，分数从小到大排序；下标为 index 的成员。
func (s *_zset) GetByIndex(index int64) (ZsetItem, error) {
	s.RWL.RLock()
	result, err := s.Redis.ZRangeWithScores(context.Background(), s.TopicKey(), index, index).Result()
	s.RWL.RUnlock()
	if len(result) <= 0 {
		return ZsetItem{}, fmt.Errorf(fmt.Sprint(err))
	}
	return ZsetItem{
		Key:   fmt.Sprint(result[0].Member),
		Score: result[0].Score,
	}, err
}

// GetByIndexRange 获取指定分数排名的成员，分数从小到大排序；下标为 start - end 的成员。
func (s *_zset) GetByIndexRange(start int64, end int64) ([]ZsetItem, error) {
	s.RWL.RLock()
	result, err := s.Redis.ZRangeWithScores(context.Background(), s.TopicKey(), start, end).Result()
	s.RWL.RUnlock()
	if len(result) <= 0 {
		return nil, fmt.Errorf(fmt.Sprint(err))
	}
	var arr []ZsetItem
	for _, d := range result {
		arr = append(arr, ZsetItem{
			Key:   fmt.Sprint(d.Member),
			Score: d.Score,
		})
	}
	return arr, err
}

// GetByIndexDESC 获取指定分数排名的成员，分数从大到小排序；下标为 index 的成员。
func (s *_zset) GetByIndexDESC(index int64) (ZsetItem, error) {
	s.RWL.RLock()
	result, err := s.Redis.ZRevRangeWithScores(context.Background(), s.TopicKey(), index, index).Result()
	s.RWL.RUnlock()
	if len(result) <= 0 {
		return ZsetItem{}, fmt.Errorf(fmt.Sprint(err))
	}
	return ZsetItem{
		Key:   fmt.Sprint(result[0].Member),
		Score: result[0].Score,
	}, err
}

// GetByIndexRangeDESC 获取指定分数排名的成员，分数从大到小排序；下标为 start - end 的成员。
func (s *_zset) GetByIndexRangeDESC(start int64, end int64) ([]ZsetItem, error) {
	s.RWL.RLock()
	result, err := s.Redis.ZRevRangeWithScores(context.Background(), s.TopicKey(), start, end).Result()
	s.RWL.RUnlock()
	if len(result) <= 0 {
		return nil, fmt.Errorf(fmt.Sprint(err))
	}
	var arr []ZsetItem
	for _, d := range result {
		arr = append(arr, ZsetItem{
			Key:   fmt.Sprint(d.Member),
			Score: d.Score,
		})
	}
	return arr, err
}

// GetByKey 获取指定key的成员
func (s *_zset) GetByKey(key string) (ZsetItem, error) {
	s.RWL.RLock()
	result, err := s.Redis.ZScore(context.Background(), s.TopicKey(), key).Result()
	s.RWL.RUnlock()
	return ZsetItem{
		Key:   key,
		Score: result,
	}, err
}

// Count 获取集合的成员数量
func (s *_zset) Count() (int64, error) {
	s.RWL.RLock()
	defer s.RWL.RUnlock()
	result, err := s.Redis.ZCard(context.Background(), s.TopicKey()).Result()
	if err == nil || err == redis.Nil {
		return result, nil
	}
	return result, err
}

// GetAllKey 获取所有的key
func (s *_zset) GetAllKey() ([]ZsetItem, error) {
	s.RWL.RLock()
	result, err := s.Redis.ZRangeWithScores(context.Background(), s.TopicKey(), 0, -1).Result()
	s.RWL.RUnlock()
	if len(result) <= 0 {
		return nil, fmt.Errorf(fmt.Sprint(err))
	}
	var arr []ZsetItem
	for _, d := range result {
		arr = append(arr, ZsetItem{
			Key:   fmt.Sprint(d.Member),
			Score: d.Score,
		})
	}
	return arr, err
}

// ClearAll 清空所有的记录（直接删除key）
func (s *_zset) ClearAll() error {
	s.RWL.Lock()
	err := s.Redis.Del(context.Background(), s.TopicKey()).Err()
	s.RWL.Unlock()
	return err
}
