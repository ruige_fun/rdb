package rdb

import (
	"context"
	"fmt"
	"github.com/redis/go-redis/v9"
	"strconv"
	"sync"
)

type MapInt64List interface {
	TopicKey(id any) string
	RedisClient() *redis.Client
	Set(id any, key string, value int64) error
	Sets(id any, kv map[string]int64) error
	Get(id any, key string) (int64, error)
	Gets(id any, keys []string) (map[string]int64, error)
	GetAll(id any) (map[string]int64, error)
	Del(id any, key string) error
	Dels(id any, key []string) error
	Count(id any) (int64, error)
	Scan(id any, dest interface{}) error
	ClearAll(id any) error
}

type _mapInt64List struct {
	Topic string        //主题
	Redis *redis.Client //Redis客户端
	RWL   sync.RWMutex  //读写锁
}

func NewMapInt64List(client *redis.Client, topic string) MapInt64List {
	return &_mapInt64List{
		Topic: topic,
		Redis: client,
		RWL:   sync.RWMutex{},
	}
}

// TopicKey 返回默认生成的主题key
func (m *_mapInt64List) TopicKey(id any) string {
	return fmt.Sprint("MapInt64List_", m.Topic, "_", id)
}

// RedisClient 返回绑定的Redis客户端
func (m *_mapInt64List) RedisClient() *redis.Client {
	return m.Redis
}

func (m *_mapInt64List) Set(id any, key string, value int64) error {
	m.RWL.Lock()
	err := m.Redis.HSet(context.Background(), m.TopicKey(id), []any{key, value}).Err()
	m.RWL.Unlock()
	return err
}

func (m *_mapInt64List) Sets(id any, kv map[string]int64) error {
	m.RWL.Lock()
	var temp = make([]any, 0, len(kv)*2)
	for k, v := range kv {
		temp = append(temp, k)
		temp = append(temp, v)
	}
	err := m.Redis.HMSet(context.Background(), m.TopicKey(id), temp...).Err()
	m.RWL.Unlock()
	return err
}

func (m *_mapInt64List) Get(id any, key string) (int64, error) {
	m.RWL.RLock()
	result, err := m.Redis.HGet(context.Background(), m.TopicKey(id), key).Int64()
	m.RWL.RUnlock()
	return result, err
}

func (m *_mapInt64List) Gets(id any, keys []string) (map[string]int64, error) {
	m.RWL.RLock()
	result, err := m.Redis.HMGet(context.Background(), m.TopicKey(id), keys...).Result()
	m.RWL.RUnlock()
	var temp = make(map[string]int64)
	for i := 0; i < len(keys); i++ {
		if result[i] != nil {
			parseInt, _ := strconv.ParseInt(fmt.Sprint(result[i]), 10, 64)
			temp[keys[i]] = parseInt
		}
	}
	return temp, err
}

func (m *_mapInt64List) GetAll(id any) (map[string]int64, error) {
	m.RWL.RLock()
	result, err := m.Redis.HGetAll(context.Background(), m.TopicKey(id)).Result()
	m.RWL.RUnlock()
	var temp = make(map[string]int64)
	for k, v := range result {
		parseInt, _ := strconv.ParseInt(fmt.Sprint(v), 10, 64)
		temp[k] = parseInt
	}
	return temp, err
}

func (m *_mapInt64List) Del(id any, key string) error {
	m.RWL.Lock()
	err := m.Redis.HDel(context.Background(), m.TopicKey(id), key).Err()
	m.RWL.Unlock()
	return err
}

func (m *_mapInt64List) Dels(id any, key []string) error {
	m.RWL.Lock()
	err := m.Redis.HDel(context.Background(), m.TopicKey(id), key...).Err()
	m.RWL.Unlock()
	return err
}

// Count 获取哈希表中字段的数量
func (m *_mapInt64List) Count(id any) (int64, error) {
	m.RWL.RLock()
	defer m.RWL.RUnlock()
	result, err := m.Redis.HLen(context.Background(), m.TopicKey(id)).Result()
	if err == nil || err == redis.Nil {
		return result, nil
	}
	return result, err
}

// Scan 将结果扫描到目标结构体中
func (m *_mapInt64List) Scan(id any, dest interface{}) error {
	m.RWL.RLock()
	err := m.Redis.HGetAll(context.Background(), m.TopicKey(id)).Scan(dest)
	m.RWL.RUnlock()
	return err
}

// ClearAll 清空所有的记录（直接删除key）
func (m *_mapInt64List) ClearAll(id any) error {
	m.RWL.Lock()
	err := m.Redis.Del(context.Background(), m.TopicKey(id)).Err()
	m.RWL.Unlock()
	return err
}
