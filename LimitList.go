package rdb

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"sync"
	"time"

	"github.com/redis/go-redis/v9"
)

type MapLimitList interface {
	TopicKey(id any) string
	RedisClient() *redis.Client
	Add(id any, key string, delta uint) error
	Sub(id any, key string, delta uint) error
	GetAndAdd(id any, key string, delta uint) (int64, error)
	GetAndSub(id any, key string, delta uint) (int64, error)
	Set(id any, key string, value uint) error
	SetNX(id any, key string, value uint) error
	Get(id any, key string) (int64, error)
	Del(id any, key string) error
	Reset(id any, key string) error
	IsExist(id any, key string) (bool, error)
	IsToLimit(id any, key string, max int64) bool
	Count(id any) (int64, error)
	GetAll(id any) (map[string]int64, error)
	Scan(id any, dest interface{}) error
	ClearAll(id any) error
}

type _mapLimitList struct {
	Topic string        //主题
	Redis *redis.Client //Redis客户端
	RWL   sync.RWMutex  //读写锁
}

func NewLimitList(client *redis.Client, topic string) MapLimitList {
	return &_mapLimitList{
		Topic: topic,
		Redis: client,
		RWL:   sync.RWMutex{},
	}
}

// TopicKey 返回默认生成的主题key
func (l *_mapLimitList) TopicKey(id any) string {
	return fmt.Sprint("MapLimitList_", l.Topic, "_", id)
}

// RedisClient 返回绑定的Redis客户端
func (l *_mapLimitList) RedisClient() *redis.Client {
	return l.Redis
}

func (l *_mapLimitList) Add(id any, key string, delta uint) error {
	l.RWL.Lock()
	err := l.Redis.HIncrBy(context.Background(), l.TopicKey(id), key, int64(delta)).Err()
	l.RWL.Unlock()
	return err
}

func (l *_mapLimitList) Sub(id any, key string, delta uint) error {
	l.RWL.Lock()
	err := l.Redis.HIncrBy(context.Background(), l.TopicKey(id), key, int64(-delta)).Err()
	l.RWL.Unlock()
	return err
}

func (l *_mapLimitList) GetAndAdd(id any, key string, delta uint) (int64, error) {
	l.RWL.Lock()
	v, err := l.Redis.HIncrBy(context.Background(), l.TopicKey(id), key, int64(delta)).Result()
	l.RWL.Unlock()
	return v, err
}

func (l *_mapLimitList) GetAndSub(id any, key string, delta uint) (int64, error) {
	l.RWL.Lock()
	v, err := l.Redis.HIncrBy(context.Background(), l.TopicKey(id), key, int64(-delta)).Result()
	l.RWL.Unlock()
	return v, err
}

func (l *_mapLimitList) Set(id any, key string, value uint) error {
	l.RWL.Lock()
	err := l.Redis.HSet(context.Background(), l.TopicKey(id), key, value).Err()
	l.RWL.Unlock()
	return err
}

func (l *_mapLimitList) SetNX(id any, key string, value uint) error {
	l.RWL.Lock()
	err := l.Redis.HSetNX(context.Background(), l.TopicKey(id), key, value).Err()
	l.RWL.Unlock()
	return err
}

func (l *_mapLimitList) Get(id any, key string) (int64, error) {
	l.RWL.RLock()
	defer l.RWL.RUnlock()
	result, err := l.Redis.HGet(context.Background(), l.TopicKey(id), key).Int64()
	if err == nil || errors.Is(err, redis.Nil) {
		return result, nil
	}
	return result, err
}

func (l *_mapLimitList) Del(id any, key string) error {
	l.RWL.Lock()
	err := l.Redis.HDel(context.Background(), l.TopicKey(id), key).Err()
	l.RWL.Unlock()
	return err
}

func (l *_mapLimitList) Reset(id any, key string) error {
	l.RWL.Lock()
	err := l.Redis.HSet(context.Background(), l.TopicKey(id), key, 0).Err()
	l.RWL.Unlock()
	return err
}

func (l *_mapLimitList) IsExist(id any, key string) (bool, error) {
	l.RWL.Lock()
	bl, err := l.Redis.HExists(context.Background(), l.TopicKey(id), key).Result()
	l.RWL.Unlock()
	return bl, err
}

func (l *_mapLimitList) IsToLimit(id any, key string, max int64) bool {
	l.RWL.RLock()
	var result int64 = 0
	for i := 0; i < 10; i++ {
		temp, err := l.Redis.HGet(context.Background(), l.TopicKey(id), key).Int64()
		if err == nil {
			result = temp
			break
		}
		if errors.Is(err, redis.Nil) {
			result = 0
			break
		}
		time.Sleep(time.Millisecond * 100)
	}
	l.RWL.RUnlock()
	return result >= max
}

// Count 获取哈希表中字段的数量
func (l *_mapLimitList) Count(id any) (int64, error) {
	l.RWL.RLock()
	defer l.RWL.RUnlock()
	result, err := l.Redis.HLen(context.Background(), l.TopicKey(id)).Result()
	if err == nil || errors.Is(err, redis.Nil) {
		return result, nil
	}
	return result, err
}

// GetAll 获取所有的key
func (l *_mapLimitList) GetAll(id any) (map[string]int64, error) {
	l.RWL.RLock()
	result, err := l.Redis.HGetAll(context.Background(), l.TopicKey(id)).Result()
	l.RWL.RUnlock()
	var temp = make(map[string]int64)
	for k, v := range result {
		parseInt, _ := strconv.ParseInt(fmt.Sprint(v), 10, 64)
		temp[k] = parseInt
	}
	return temp, err
}

// Scan 将结果扫描到目标结构体中
func (l *_mapLimitList) Scan(id any, dest interface{}) error {
	l.RWL.RLock()
	err := l.Redis.HGetAll(context.Background(), l.TopicKey(id)).Scan(dest)
	l.RWL.RUnlock()
	return err
}

// ClearAll 清空所有的记录（直接删除key）
func (l *_mapLimitList) ClearAll(id any) error {
	l.RWL.Lock()
	err := l.Redis.Del(context.Background(), l.TopicKey(id)).Err()
	l.RWL.Unlock()
	return err
}
