package rdb

import (
	"context"
	"fmt"
	"strconv"
	"sync"

	"github.com/redis/go-redis/v9"
)

type QueueInt64List interface {
	TopicKey(id any) string
	RedisClient() *redis.Client
	SetQueueInt64ListMaxLength(max uint64)
	LPush(id any, value int64) error
	RPush(id any, value int64) error
	LPushList(id any, list []int64) error
	RPushList(id any, list []int64) error
	LPop(id any) (int64, error)
	RPop(id any) (int64, error)
	LPopList(id any, count int) ([]int64, error)
	RPopList(id any, count int) ([]int64, error)
	Count(id any) (int64, error)
	GetAll(id any) ([]int64, error)
	ClearAll(id any) error
}

type _queueInt64List struct {
	Topic     string        //主题
	Redis     *redis.Client //Redis客户端
	MaxLength uint64        //队列最大容量
	RWL       sync.RWMutex  //读写锁
}

func NewQueueInt64List(client *redis.Client, topic string) QueueInt64List {
	return &_queueInt64List{
		Topic: topic,
		Redis: client,
		RWL:   sync.RWMutex{},
	}
}

func NewQueueInt64ListAndMax(client *redis.Client, topic string, max uint64) QueueInt64List {
	rt := &_queueInt64List{
		Topic: topic,
		Redis: client,
		RWL:   sync.RWMutex{},
	}
	rt.SetQueueInt64ListMaxLength(max)
	return rt
}

// TopicKey 返回默认生成的主题key
func (q *_queueInt64List) TopicKey(id any) string {
	return fmt.Sprint("QueueInt64List_", q.Topic, "_", id)
}

// RedisClient 返回绑定的Redis客户端
func (q *_queueInt64List) RedisClient() *redis.Client {
	return q.Redis
}

// SetQueueInt64ListMaxLength 队列最大容量长度，0则无限长。
// 超出这个长度的时候，插入数据的同时，会往返方向移出，直到剩余长度等于最大容量长度。
func (q *_queueInt64List) SetQueueInt64ListMaxLength(max uint64) {
	q.RWL.Lock()
	q.MaxLength = max
	q.RWL.Unlock()
}

// LPush 左边插入 1 个
func (q *_queueInt64List) LPush(id any, value int64) error {
	q.RWL.Lock()
	err := q.Redis.LPush(context.Background(), q.TopicKey(id), value).Err()
	if q.MaxLength > 0 && err == nil {
		result, _ := q.Redis.LLen(context.Background(), q.TopicKey(id)).Result()
		if uint64(result)-q.MaxLength > 0 {
			for i := 0; i < int(uint64(result)-q.MaxLength); i++ {
				q.Redis.RPop(context.Background(), q.TopicKey(id))
			}
		}
	}
	q.RWL.Unlock()
	return err
}

// RPush 右边插入 1 个
func (q *_queueInt64List) RPush(id any, value int64) error {
	q.RWL.Lock()
	err := q.Redis.RPush(context.Background(), q.TopicKey(id), value).Err()
	if q.MaxLength > 0 && err == nil {
		result, _ := q.Redis.LLen(context.Background(), q.TopicKey(id)).Result()
		if uint64(result)-q.MaxLength > 0 {
			for i := 0; i < int(uint64(result)-q.MaxLength); i++ {
				q.Redis.LPop(context.Background(), q.TopicKey(id))
			}
		}
	}
	q.RWL.Unlock()
	return err
}

// LPushList 左边插入 N 个
func (q *_queueInt64List) LPushList(id any, list []int64) error {
	q.RWL.Lock()
	var temp []interface{}
	for _, d := range list {
		temp = append(temp, d)
	}
	err := q.Redis.LPush(context.Background(), q.TopicKey(id), temp...).Err()
	if q.MaxLength > 0 && err == nil {
		result, _ := q.Redis.LLen(context.Background(), q.TopicKey(id)).Result()
		if uint64(result)+uint64(len(list))-q.MaxLength > 0 {
			for i := 0; i < int(uint64(result)+uint64(len(list))-q.MaxLength); i++ {
				q.Redis.RPop(context.Background(), q.TopicKey(id))
			}
		}
	}
	q.RWL.Unlock()
	return err
}

// RPushList 右边插入 N 个
func (q *_queueInt64List) RPushList(id any, list []int64) error {
	q.RWL.Lock()
	var temp []interface{}
	for _, d := range list {
		temp = append(temp, d)
	}
	err := q.Redis.RPush(context.Background(), q.TopicKey(id), temp...).Err()
	if q.MaxLength > 0 && err == nil {
		result, _ := q.Redis.LLen(context.Background(), q.TopicKey(id)).Result()
		if uint64(result)+uint64(len(list))-q.MaxLength > 0 {
			for i := 0; i < int(uint64(result)+uint64(len(list))-q.MaxLength); i++ {
				q.Redis.LPop(context.Background(), q.TopicKey(id))
			}
		}
	}
	q.RWL.Unlock()
	return err
}

// LPop 左边移出 1 个
func (q *_queueInt64List) LPop(id any) (int64, error) {
	q.RWL.Lock()
	result, err := q.Redis.LPop(context.Background(), q.TopicKey(id)).Result()
	q.RWL.Unlock()
	val, _ := strconv.ParseInt(result, 10, 64)
	return val, err
}

// RPop 右边移出 1 个
func (q *_queueInt64List) RPop(id any) (int64, error) {
	q.RWL.Lock()
	result, err := q.Redis.RPop(context.Background(), q.TopicKey(id)).Result()
	q.RWL.Unlock()
	val, _ := strconv.ParseInt(result, 10, 64)
	return val, err
}

// LPopList 左边移出 N 个
func (q *_queueInt64List) LPopList(id any, count int) ([]int64, error) {
	q.RWL.Lock()
	var result []int64
	var j = 0
	for i := 0; i < count; i++ {
		j++
		temp, err := q.Redis.LPop(context.Background(), q.TopicKey(id)).Result()
		if err != nil || j > count*2 {
			if err == redis.Nil || j > count*2 {
				break
			}
			i--
			continue
		}
		val, _ := strconv.ParseInt(temp, 10, 64)
		result = append(result, val)
	}
	q.RWL.Unlock()
	if len(result) <= 0 {
		return result, fmt.Errorf("empty")
	}
	return result, nil
}

// RPopList 右边移出 N 个
func (q *_queueInt64List) RPopList(id any, count int) ([]int64, error) {
	q.RWL.Lock()
	var result []int64
	var j = 0
	for i := 0; i < count; i++ {
		j++
		temp, err := q.Redis.RPop(context.Background(), q.TopicKey(id)).Result()
		if err != nil || j > count*2 {
			if err == redis.Nil || j > count*2 {
				break
			}
			i--
			continue
		}
		val, _ := strconv.ParseInt(temp, 10, 64)
		result = append(result, val)
	}
	q.RWL.Unlock()
	if len(result) <= 0 {
		return result, fmt.Errorf("empty")
	}
	return result, nil
}

// Count 获取队列当前长度
func (q *_queueInt64List) Count(id any) (int64, error) {
	q.RWL.RLock()
	defer q.RWL.RUnlock()
	result, err := q.Redis.LLen(context.Background(), q.TopicKey(id)).Result()
	if err == nil || err == redis.Nil {
		return result, nil
	}
	return result, err
}

// GetAll 获取队列中所有的记录
func (q *_queueInt64List) GetAll(id any) ([]int64, error) {
	q.RWL.RLock()
	result, err := q.Redis.LRange(context.Background(), q.TopicKey(id), 0, -1).Result()
	q.RWL.RUnlock()
	var temp []int64
	for _, d := range result {
		val, _ := strconv.ParseInt(d, 10, 64)
		temp = append(temp, val)
	}
	return temp, err
}

// ClearAll 清空所有的记录（直接删除key）
func (q *_queueInt64List) ClearAll(id any) error {
	q.RWL.Lock()
	err := q.Redis.Del(context.Background(), q.TopicKey(id)).Err()
	q.RWL.Unlock()
	return err
}
