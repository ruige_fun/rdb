package rdb

import (
	"context"
	"encoding/json"
	"fmt"
	"sync"

	"github.com/redis/go-redis/v9"
)

type MapStringList interface {
	TopicKey(id any) string
	RedisClient() *redis.Client
	Set(id any, key string, value string) error
	Sets(id any, kv map[string]string) error
	SetJson(id any, key string, pointer any) error
	GetJson(id any, key string, pointer any) error
	Get(id any, key string) (string, error)
	Gets(id any, keys []string) (map[string]string, error)
	GetAll(id any) (map[string]string, error)
	Del(id any, key string) error
	Dels(id any, key []string) error
	Count(id any) (int64, error)
	Scan(id any, dest interface{}) error
	ClearAll(id any) error
}

type _mapStringList struct {
	Topic string        //主题
	Redis *redis.Client //Redis客户端
	RWL   sync.RWMutex  //读写锁
}

func NewMapStringList(client *redis.Client, topic string) MapStringList {
	return &_mapStringList{
		Topic: topic,
		Redis: client,
		RWL:   sync.RWMutex{},
	}
}

// TopicKey 返回默认生成的主题key
func (m *_mapStringList) TopicKey(id any) string {
	return fmt.Sprint("MapStringList_", m.Topic, "_", id)
}

// RedisClient 返回绑定的Redis客户端
func (m *_mapStringList) RedisClient() *redis.Client {
	return m.Redis
}

func (m *_mapStringList) Set(id any, key string, value string) error {
	m.RWL.Lock()
	err := m.Redis.HSet(context.Background(), m.TopicKey(id), key, value).Err()
	m.RWL.Unlock()
	return err
}

func (m *_mapStringList) Sets(id any, kv map[string]string) error {
	m.RWL.Lock()
	var temp = make([]any, 0, len(kv)*2)
	for k, v := range kv {
		temp = append(temp, k)
		temp = append(temp, v)
	}
	err := m.Redis.HMSet(context.Background(), m.TopicKey(id), temp...).Err()
	m.RWL.Unlock()
	return err
}

func (m *_mapStringList) SetJson(id any, key string, pointer any) error {
	marshal, err := json.Marshal(pointer)
	if err != nil {
		return err
	}
	m.RWL.Lock()
	err = m.Redis.HSet(context.Background(), m.TopicKey(id), key, string(marshal)).Err()
	m.RWL.Unlock()
	return err
}

func (m *_mapStringList) GetJson(id any, key string, pointer any) error {
	m.RWL.RLock()
	result, err := m.Redis.HGet(context.Background(), m.TopicKey(id), key).Result()
	m.RWL.RUnlock()
	if err != nil {
		return err
	}
	err = json.Unmarshal([]byte(result), pointer)
	return err
}

func (m *_mapStringList) Get(id any, key string) (string, error) {
	m.RWL.RLock()
	result, err := m.Redis.HGet(context.Background(), m.TopicKey(id), key).Result()
	m.RWL.RUnlock()
	return result, err
}

func (m *_mapStringList) Gets(id any, keys []string) (map[string]string, error) {
	m.RWL.RLock()
	result, err := m.Redis.HMGet(context.Background(), m.TopicKey(id), keys...).Result()
	m.RWL.RUnlock()
	var temp = make(map[string]string)
	for i := 0; i < len(keys); i++ {
		if result[i] != nil {
			temp[keys[i]] = fmt.Sprint(result[i])
		}
	}
	return temp, err
}

func (m *_mapStringList) GetAll(id any) (map[string]string, error) {
	m.RWL.RLock()
	result, err := m.Redis.HGetAll(context.Background(), m.TopicKey(id)).Result()
	m.RWL.RUnlock()
	return result, err
}

func (m *_mapStringList) Del(id any, key string) error {
	m.RWL.Lock()
	err := m.Redis.HDel(context.Background(), m.TopicKey(id), key).Err()
	m.RWL.Unlock()
	return err
}

func (m *_mapStringList) Dels(id any, key []string) error {
	m.RWL.Lock()
	err := m.Redis.HDel(context.Background(), m.TopicKey(id), key...).Err()
	m.RWL.Unlock()
	return err
}

// Count 获取哈希表中字段的数量
func (m *_mapStringList) Count(id any) (int64, error) {
	m.RWL.RLock()
	defer m.RWL.RUnlock()
	result, err := m.Redis.HLen(context.Background(), m.TopicKey(id)).Result()
	if err == nil || err == redis.Nil {
		return result, nil
	}
	return result, err
}

// Scan 将结果扫描到目标结构体中
func (m *_mapStringList) Scan(id any, dest interface{}) error {
	m.RWL.RLock()
	err := m.Redis.HGetAll(context.Background(), m.TopicKey(id)).Scan(dest)
	m.RWL.RUnlock()
	return err
}

// ClearAll 清空所有的记录（直接删除key）
func (m *_mapStringList) ClearAll(id any) error {
	m.RWL.Lock()
	err := m.Redis.Del(context.Background(), m.TopicKey(id)).Err()
	m.RWL.Unlock()
	return err
}
