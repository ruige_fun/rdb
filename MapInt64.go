package rdb

import (
	"context"
	"fmt"
	"github.com/redis/go-redis/v9"
	"strconv"
	"sync"
)

type MapInt64 interface {
	TopicKey() string
	RedisClient() *redis.Client
	Set(key string, value int64) error
	Sets(kv map[string]int64) error
	Get(key string) (int64, error)
	Gets(keys []string) (map[string]int64, error)
	GetAll() (map[string]int64, error)
	Del(key string) error
	Dels(key []string) error
	Count() (int64, error)
	Scan(dest interface{}) error
	ClearAll() error
}

type _mapInt64 struct {
	Topic string        //主题
	Redis *redis.Client //Redis客户端
	RWL   sync.RWMutex  //读写锁
}

func NewMapInt64(client *redis.Client, topic string) MapInt64 {
	return &_mapInt64{
		Topic: topic,
		Redis: client,
		RWL:   sync.RWMutex{},
	}
}

// TopicKey 返回默认生成的主题key
func (m *_mapInt64) TopicKey() string {
	return fmt.Sprint("MapInt64_", m.Topic)
}

// RedisClient 返回绑定的Redis客户端
func (m *_mapInt64) RedisClient() *redis.Client {
	return m.Redis
}

func (m *_mapInt64) Set(key string, value int64) error {
	m.RWL.Lock()
	err := m.Redis.HSet(context.Background(), m.TopicKey(), []any{key, value}).Err()
	m.RWL.Unlock()
	return err
}

func (m *_mapInt64) Sets(kv map[string]int64) error {
	m.RWL.Lock()
	var temp = make([]any, 0, len(kv)*2)
	for k, v := range kv {
		temp = append(temp, k)
		temp = append(temp, v)
	}
	err := m.Redis.HMSet(context.Background(), m.TopicKey(), temp...).Err()
	m.RWL.Unlock()
	return err
}

func (m *_mapInt64) Get(key string) (int64, error) {
	m.RWL.RLock()
	result, err := m.Redis.HGet(context.Background(), m.TopicKey(), key).Int64()
	m.RWL.RUnlock()
	return result, err
}

func (m *_mapInt64) Gets(keys []string) (map[string]int64, error) {
	m.RWL.RLock()
	result, err := m.Redis.HMGet(context.Background(), m.TopicKey(), keys...).Result()
	m.RWL.RUnlock()
	var temp = make(map[string]int64)
	for i := 0; i < len(keys); i++ {
		if result[i] != nil {
			parseInt, _ := strconv.ParseInt(fmt.Sprint(result[i]), 10, 64)
			temp[keys[i]] = parseInt
		}
	}
	return temp, err
}

func (m *_mapInt64) GetAll() (map[string]int64, error) {
	m.RWL.RLock()
	result, err := m.Redis.HGetAll(context.Background(), m.TopicKey()).Result()
	m.RWL.RUnlock()
	var temp = make(map[string]int64)
	for k, v := range result {
		parseInt, _ := strconv.ParseInt(fmt.Sprint(v), 10, 64)
		temp[k] = parseInt
	}
	return temp, err
}

func (m *_mapInt64) Del(key string) error {
	m.RWL.Lock()
	err := m.Redis.HDel(context.Background(), m.TopicKey(), key).Err()
	m.RWL.Unlock()
	return err
}

func (m *_mapInt64) Dels(key []string) error {
	m.RWL.Lock()
	err := m.Redis.HDel(context.Background(), m.TopicKey(), key...).Err()
	m.RWL.Unlock()
	return err
}

// Count 获取哈希表中字段的数量
func (m *_mapInt64) Count() (int64, error) {
	m.RWL.RLock()
	defer m.RWL.RUnlock()
	result, err := m.Redis.HLen(context.Background(), m.TopicKey()).Result()
	if err == nil || err == redis.Nil {
		return result, nil
	}
	return result, err
}

// Scan 将结果扫描到目标结构体中
func (m *_mapInt64) Scan(dest interface{}) error {
	m.RWL.RLock()
	err := m.Redis.HGetAll(context.Background(), m.TopicKey()).Scan(dest)
	m.RWL.RUnlock()
	return err
}

// ClearAll 清空所有的记录（直接删除key）
func (m *_mapInt64) ClearAll() error {
	m.RWL.Lock()
	err := m.Redis.Del(context.Background(), m.TopicKey()).Err()
	m.RWL.Unlock()
	return err
}
