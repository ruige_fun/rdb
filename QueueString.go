package rdb

import (
	"context"
	"fmt"
	"sync"

	"github.com/redis/go-redis/v9"
)

type QueueString interface {
	TopicKey() string
	RedisClient() *redis.Client
	SetQueueStringMaxLength(max uint64)
	LPush(value string) error
	RPush(value string) error
	LPushList(list []string) error
	RPushList(list []string) error
	LPop() (string, error)
	RPop() (string, error)
	LPopList(count int) ([]string, error)
	RPopList(count int) ([]string, error)
	Count() (int64, error)
	GetAll() ([]string, error)
	ClearAll() error
}

type _queueString struct {
	Topic     string        //主题
	Redis     *redis.Client //Redis客户端
	MaxLength uint64        //队列最大容量
	RWL       sync.RWMutex  //读写锁
}

func NewQueueString(client *redis.Client, topic string) QueueString {
	return &_queueString{
		Topic: topic,
		Redis: client,
		RWL:   sync.RWMutex{},
	}
}

func NewQueueStringAndMax(client *redis.Client, topic string, max uint64) QueueString {
	rt := &_queueString{
		Topic: topic,
		Redis: client,
		RWL:   sync.RWMutex{},
	}
	rt.SetQueueStringMaxLength(max)
	return rt
}

// TopicKey 返回默认生成的主题key
func (q *_queueString) TopicKey() string {
	return fmt.Sprint("QueueString_", q.Topic)
}

// RedisClient 返回绑定的Redis客户端
func (q *_queueString) RedisClient() *redis.Client {
	return q.Redis
}

// SetQueueStringMaxLength 队列最大容量长度，0则无限长。
// 超出这个长度的时候，插入数据的同时，会往返方向移出，直到剩余长度等于最大容量长度。
func (q *_queueString) SetQueueStringMaxLength(max uint64) {
	q.RWL.Lock()
	q.MaxLength = max
	q.RWL.Unlock()
}

// LPush 左边插入 1 个
func (q *_queueString) LPush(value string) error {
	q.RWL.Lock()
	err := q.Redis.LPush(context.Background(), q.TopicKey(), value).Err()
	if q.MaxLength > 0 && err == nil {
		result, _ := q.Redis.LLen(context.Background(), q.TopicKey()).Result()
		if uint64(result)-q.MaxLength > 0 {
			for i := 0; i < int(uint64(result)-q.MaxLength); i++ {
				q.Redis.RPop(context.Background(), q.TopicKey())
			}
		}
	}
	q.RWL.Unlock()
	return err
}

// RPush 右边插入 1 个
func (q *_queueString) RPush(value string) error {
	q.RWL.Lock()
	err := q.Redis.RPush(context.Background(), q.TopicKey(), value).Err()
	if q.MaxLength > 0 && err == nil {
		result, _ := q.Redis.LLen(context.Background(), q.TopicKey()).Result()
		if uint64(result)-q.MaxLength > 0 {
			for i := 0; i < int(uint64(result)-q.MaxLength); i++ {
				q.Redis.LPop(context.Background(), q.TopicKey())
			}
		}
	}
	q.RWL.Unlock()
	return err
}

// LPushList 左边插入 N 个
func (q *_queueString) LPushList(list []string) error {
	q.RWL.Lock()
	var temp []interface{}
	for _, d := range list {
		temp = append(temp, d)
	}
	err := q.Redis.LPush(context.Background(), q.TopicKey(), temp...).Err()
	if q.MaxLength > 0 && err == nil {
		result, _ := q.Redis.LLen(context.Background(), q.TopicKey()).Result()
		if uint64(result)+uint64(len(list))-q.MaxLength > 0 {
			for i := 0; i < int(uint64(result)+uint64(len(list))-q.MaxLength); i++ {
				q.Redis.RPop(context.Background(), q.TopicKey())
			}
		}
	}
	q.RWL.Unlock()
	return err
}

// RPushList 右边插入 N 个
func (q *_queueString) RPushList(list []string) error {
	q.RWL.Lock()
	var temp []interface{}
	for _, d := range list {
		temp = append(temp, d)
	}
	err := q.Redis.RPush(context.Background(), q.TopicKey(), temp...).Err()
	if q.MaxLength > 0 && err == nil {
		result, _ := q.Redis.LLen(context.Background(), q.TopicKey()).Result()
		if uint64(result)+uint64(len(list))-q.MaxLength > 0 {
			for i := 0; i < int(uint64(result)+uint64(len(list))-q.MaxLength); i++ {
				q.Redis.LPop(context.Background(), q.TopicKey())
			}
		}
	}
	q.RWL.Unlock()
	return err
}

// LPop 左边移出 1 个
func (q *_queueString) LPop() (string, error) {
	q.RWL.Lock()
	result, err := q.Redis.LPop(context.Background(), q.TopicKey()).Result()
	q.RWL.Unlock()
	return result, err
}

// RPop 右边移出 1 个
func (q *_queueString) RPop() (string, error) {
	q.RWL.Lock()
	result, err := q.Redis.RPop(context.Background(), q.TopicKey()).Result()
	q.RWL.Unlock()
	return result, err
}

// LPopList 左边移出 N 个
func (q *_queueString) LPopList(count int) ([]string, error) {
	q.RWL.Lock()
	var result []string
	var j = 0
	for i := 0; i < count; i++ {
		j++
		temp, err := q.Redis.LPop(context.Background(), q.TopicKey()).Result()
		if err != nil || j > count*2 {
			if err == redis.Nil || j > count*2 {
				break
			}
			i--
			continue
		}
		result = append(result, temp)
	}
	q.RWL.Unlock()
	if len(result) <= 0 {
		return result, fmt.Errorf("empty")
	}
	return result, nil
}

// RPopList 右边移出 N 个
func (q *_queueString) RPopList(count int) ([]string, error) {
	q.RWL.Lock()
	var result []string
	var j = 0
	for i := 0; i < count; i++ {
		j++
		temp, err := q.Redis.RPop(context.Background(), q.TopicKey()).Result()
		if err != nil || j > count*2 {
			if err == redis.Nil || j > count*2 {
				break
			}
			i--
			continue
		}
		result = append(result, temp)
	}
	q.RWL.Unlock()
	if len(result) <= 0 {
		return result, fmt.Errorf("empty")
	}
	return result, nil
}

// Count 获取队列当前长度
func (q *_queueString) Count() (int64, error) {
	q.RWL.RLock()
	defer q.RWL.RUnlock()
	result, err := q.Redis.LLen(context.Background(), q.TopicKey()).Result()
	if err == nil || err == redis.Nil {
		return result, nil
	}
	return result, err
}

// GetAll 获取队列中所有的记录
func (q *_queueString) GetAll() ([]string, error) {
	q.RWL.RLock()
	result, err := q.Redis.LRange(context.Background(), q.TopicKey(), 0, -1).Result()
	q.RWL.RUnlock()
	return result, err
}

// ClearAll 清空所有的记录（直接删除key）
func (q *_queueString) ClearAll() error {
	q.RWL.Lock()
	err := q.Redis.Del(context.Background(), q.TopicKey()).Err()
	q.RWL.Unlock()
	return err
}
